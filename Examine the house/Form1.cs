﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examine_the_house
{
    public partial class Form1 : Form
    {
        Location currentLocation;

        RoomWithDoor livingRoom;
        Room diningRoom;
        RoomWithDoor kitchen;
        OutsideWithDoor frontyard;
        OutsideWithDoor backyard;
        Outside garden;

        public Form1()
        {
            InitializeComponent();
            createObjects();
            MoveToANewLocation(livingRoom);
        }

        private void createObjects()
        {
            livingRoom = new RoomWithDoor("Living room", "ancient carpet", "oak door with a brass handle");
            diningRoom = new Room("Dining room", "crystal chandelier");
            kitchen = new RoomWithDoor("Kitchen", "stainless steel cutlery", "sliding door");
            frontyard = new OutsideWithDoor("Front yard", false, "oak door with a brass handle");
            backyard = new OutsideWithDoor("Backyard", true, "sliding door");
            garden = new Outside("Garden", false);

            livingRoom.Exits = new Location[] { diningRoom };
            diningRoom.Exits = new Location[] { livingRoom, kitchen };
            kitchen.Exits = new Location[] { diningRoom };
            frontyard.Exits = new Location[] { backyard, garden };
            backyard.Exits = new Location[] { frontyard, garden };
            garden.Exits = new Location[] { frontyard, backyard };

            livingRoom.DoorLocation = frontyard;
            kitchen.DoorLocation = backyard;
            frontyard.DoorLocation = livingRoom;
            backyard.DoorLocation = kitchen;
        }

        private void MoveToANewLocation (Location newLocation)
        {
            currentLocation = newLocation;

            exitsComboBox.Items.Clear();
            foreach(var locations in currentLocation.Exits)
                exitsComboBox.Items.Add(locations.Name);
            exitsComboBox.SelectedIndex = 0;

            descriptionTextBox.Text = currentLocation.Description;

            if (currentLocation is IHasExteriorDoor)
                goThroughTheDoorButton.Visible = true;
            else
                goThroughTheDoorButton.Visible = false;
        }

        private void goHereButton_Click(object sender, EventArgs e)
        {
            MoveToANewLocation(currentLocation.Exits[exitsComboBox.SelectedIndex]);
        }

        private void goThroughTheDoorButton_Click(object sender, EventArgs e)
        {
            if (currentLocation is IHasExteriorDoor)
            {
                IHasExteriorDoor hasDoor = currentLocation as IHasExteriorDoor;
                MoveToANewLocation(hasDoor.DoorLocation);
            }
            else
                MessageBox.Show("This room don't have doors! You couldn't see this button!");
        }
    }
}
