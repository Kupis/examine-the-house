﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examine_the_house
{
    public abstract class Location
    {
        public Location(string name)
        {
            Name = name;
        }

        public Location[] Exits;
        public string Name { get; private set; }

        public virtual string Description
        {
            get
            {
                string description = "You are standing in: " + Name + ".\r\nYou can see exits to the following locations: \r\n";
                for (int i = 0; i < Exits.Length; i++)
                {
                    description += "* " + Exits[i].Name;
                    if (i != Exits.Length - 1)
                        description += ",\r\n";
                }
                description += ".\r\n";
                return description;
            }
        }
    }
}
