# Racing Simulator
.Net application showing house model. This model will be upgraded in next project - Hide&Seek.
Application is based on book "Head First C#" O'REILLY Jennifer Greene, Andrew Stellman
## Purpose
The purpose of this application is to learn more about interfaces and inherit.
## Author
* **Patryk Kupis** - [Patryk Kupis](https://gitlab.com/Kupis)
## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details